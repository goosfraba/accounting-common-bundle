<?php
namespace Webit\Accounting\CommonBundle\Service;

use Webit\Common\DictionaryBundle\Model\Dictionary\DictionaryInterface;

class PriceConfigProvider
{
    private $options;

    /**
     *
     * @var DictionaryInterface
     */
    private $currencyDictionary;

    public function __construct(array $options, DictionaryInterface $currencyDictionary)
    {
        $this->options = $options;
        $this->currencyDictionary = $currencyDictionary;
    }

    public function getDefaultCurrency()
    {
        return $this->currencyDictionary->getItem($this->options['default_currency']);
    }

    public function getPriceMode()
    {
        return $this->options['mode'];
    }
}
