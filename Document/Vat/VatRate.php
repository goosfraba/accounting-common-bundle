<?php
namespace Webit\Accounting\CommonBundle\Document\Vat;

use Doctrine\ODM\PHPCR\ChildrenCollection;
use Webit\Accounting\CommonBundle\Model\Vat\VatRateHelper;
use Webit\Accounting\CommonBundle\Model\Vat\VatRateInterface;
use Webit\Accounting\CommonBundle\Model\Vat\VatValueInterface;
use Webit\Bundle\PHPCRToolsBundle\Document\Generic;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Webit\Accounting\CommonBundle\Document\Vat\VatRate
 * @author dbojdo
 */
class VatRate extends Generic implements VatRateInterface
{
    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $label;

    /**
     * @var ArrayCollection
     */
    protected $values;

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @param VatValueInterface $value
     */
    public function addValue(VatValueInterface $value)
    {
        $value->setParent($this);
        $value->setNodename('value_' . substr(md5(mt_rand(0, 1000000) . time()), 0, 6));
        $this->getValues()->add($value);
    }

    /**
     * @param VatValueInterface $value
     */
    public function removeValue(VatValueInterface $value)
    {
        $this->getValues()->removeElement($value);
    }

    /**
     * @return ArrayCollection<VatRateValue>
     */
    public function getValues()
    {
        if ($this->values == null) {
            $this->values = new ArrayCollection();
        }

        return $this->values;
    }

    /**
     * @return VatValue|null
     */
    public function getValue(\DateTime $date = null)
    {
        return VatRateHelper::getValue($this, $date);
    }

    public function getValueLabel()
    {
        $value = $this->getValue();
        $label = $this->getCode();
        $label .= $value ? str_replace('.', ',', (sprintf(' (%.2f%%)', $value->getPercent()))) : '';

        return $label;
    }

    public function __sleep()
    {
        if ($this->getValues() instanceof ChildrenCollection) {
            $this->values = new ArrayCollection(array_values($this->getValues()->toArray()));
        }

        return array('id', 'code', 'label', 'values');
    }

    public function __toString()
    {
        return (string)$this->getCode();
    }
}
