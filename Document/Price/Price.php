<?php
namespace Webit\Accounting\CommonBundle\Document\Price;

use Webit\Common\DictionaryBundle\Annotation as Dict;
use Webit\Accounting\CommonBundle\Model\Price\PriceInterface;
use Webit\Common\CurrencyBundle\Model\CurrencyInterface;
use Webit\Common\DictionaryBundle\Model\DictionaryItem\DictionaryItemAwareInterface;
use Webit\Bundle\PHPCRToolsBundle\Document\Generic;

/**
 * Webit\Accounting\CommonBundle\Document\Vat\VatRate
 * @author dbojdo
 */
class Price extends Generic implements PriceInterface, DictionaryItemAwareInterface
{
    /**
     * @var float
     */
    protected $value;

    /**
     * @Dict\ItemCode(dictionaryName="currency",itemProperty="currency")
     */
    protected $currencyCode;

    /**
     *
     * @var CurrencyInterface
     */
    protected $currency;

    public function getValue()
    {
        return $this->value;
    }

    public function setValue($value)
    {
        $this->value = str_replace(',', '.', (string)$value);
    }

    public function getCurrency()
    {
        return $this->currency;
    }

    public function setCurrency(CurrencyInterface $currency)
    {
        $this->currency = $currency;
    }

    public function __toString()
    {
        return sprintf('%.2f %s', $this->getValue(), $this->getCurrency());
    }
}
