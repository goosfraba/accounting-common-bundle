<?php
namespace Webit\Accounting\CommonBundle\Document\Price;

use Webit\Accounting\CommonBundle\Calculator\NetGrossCalculator;
use Webit\Common\DictionaryBundle\Annotation as Dict;
use Webit\Accounting\CommonBundle\Model\Vat\VatRateInterface;
use Webit\Common\DictionaryBundle\Model\DictionaryItem\DictionaryItemAwareInterface;

/**
 * Webit\Accounting\CommonBundle\Document\Vat\VatRate
 * @author dbojdo
 */
class TaxedPrice extends Price implements DictionaryItemAwareInterface
{
    /**
     * @Dict\ItemCode(dictionaryName="vat",itemProperty="vatRate")
     */
    protected $vatRateId;

    /**
     *
     * @var VatRateInterface
     */
    protected $vatRate;

    /**
     *
     * @var bool
     */
    protected $net = false;

    /**
     * @return mixed
     */
    public function getVatRateId()
    {
        return $this->vatRateId;
    }

    /**
     * @return \Webit\Accounting\CommonBundle\Model\Vat\VatRateInterface
     */
    public function getVatRate()
    {
        return $this->vatRate;
    }

    /**
     *
     * @param VatRateInterface $vatRate
     */
    public function setVatRate(VatRateInterface $vatRate)
    {
        $this->vatRate = $vatRate;
        $this->vatRateId = $vatRate->getId();
    }

    /**
     *
     * @param bool $net
     */
    public function setNet($net)
    {
        $this->net = (bool)$net;
    }

    /**
     * @return bool
     */
    public function getNet()
    {
        return $this->net;
    }

    /**
     * @param \DateTime $date
     * @return float
     */
    public function getTaxedValue(\DateTime $date = null)
    {
        if ($this->net) {
            return NetGrossCalculator::calculateGrossPrice($this, $this->getVatRate(), $date);
        }

        return NetGrossCalculator::calculateNetPrice($this, $this->getVatRate(), $date);
    }
}
