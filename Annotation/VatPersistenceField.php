<?php
namespace Webit\Accounting\CommonBundle\Annotation;

use Doctrine\Common\Annotations\Annotation as DoctrineAnnotation;

/**
 *
 * @author dbojdo
 * @Annotation
 */
final class VatPersistenceField extends DoctrineAnnotation
{
    /** @var string */
    public $for;
}
