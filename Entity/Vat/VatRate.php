<?php
namespace Webit\Accounting\CommonBundle\Entity\Vat;

use Webit\Accounting\CommonBundle\Model\Vat\VatRateHelper;
use Webit\Accounting\CommonBundle\Model\Vat\VatRateInterface;
use Webit\Accounting\CommonBundle\Model\Vat\VatValueInterface;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Webit\Accounting\CommonBundle\Entity\Vat\VatRate
 * @author dbojdo
 */
class VatRate implements VatRateInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $label;

    /**
     * @var ArrayCollection
     */
    protected $values;

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     *
     * @param string $code
     */
    public function setSymbol($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     *
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = $label;
    }

    /**
     * @param VatValueInterface $value
     */
    public function addValue(VatValueInterface $value)
    {
        $this->getValues()->add($value);
    }

    /**
     * @param VatValueInterface $value
     */
    public function removeValue(VatValueInterface $value)
    {
        $this->getValues()->removeElement($value);
    }

    /**
     * @return ArrayCollection<VatRateValue>
     */
    public function getValues()
    {
        if ($this->values == null) {
            $this->values = new ArrayCollection();
        }

        return $this->values;
    }

    /**
     * @return VatValue|null
     */
    public function getValue(\DateTime $date = null)
    {
        return VatRateHelper::getValue($this, $date);
    }

    public function getValueLabel()
    {
        $value = $this->getValue();
        $label = $this->getCode();
        $label .= $value ? str_replace('.', ',', (sprintf(' (%.2f%%)', $value->getPercent()))) : '';

        return $label;
    }

    public function __toString()
    {
        return $this->getCode();
    }
}
