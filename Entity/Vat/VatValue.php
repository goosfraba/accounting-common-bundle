<?php
namespace Webit\Accounting\CommonBundle\Entity\Vat;

use Webit\Accounting\CommonBundle\Model\Vat\VatValueInterface;
use Webit\Accounting\CommonBundle\Model\Vat\VatRateInterface;

/**
 * Webit\Accounting\CommonBundle\Entity\Vat\VatValue
 * @author dbojdo
 */
class VatValue implements VatValueInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var float
     */
    protected $percent;

    /**
     *
     * @var \DateTime
     */
    protected $validTo;

    /**
     *
     * @var VatRate
     */
    protected $vatRate;

    /**
     * @var int
     */
    protected $vatRateCode;

    /**
     * Return float value (ex. 0.23)
     * @return float
     */
    public function getValue()
    {
        return $this->percent / 100;
    }

    /**
     *
     * @param float $value
     */
    public function setValue($value)
    {
        $this->setPercent($value * 100);
    }

    /**
     * Return percent value (ex. 23.00)
     * @return float
     */
    public function getPercent()
    {
        return $this->percent;
    }

    /**
     * @param float $percent
     */
    public function setPercent($percent)
    {
        $this->percent = round($percent, 2);
    }

    /**
     * Retrun ratio value (ex. 1.23)
     * @return float
     */
    public function getRatio()
    {
        return 1 + $this->getValue();
    }

    /**
     *
     * @param float $ratio
     */
    public function setRatio($ratio)
    {
        $this->setValue($ratio - 1);
    }

    public function getValidTo()
    {
        return $this->validTo;
    }

    public function setValidTo(\DateTime $validTo = null)
    {
        $this->validTo = $validTo;
    }

    public function setVatRate(VatRateInterface $vatRate)
    {
        $this->vatRate = $vatRate;
    }

    /**
     * @return VatRateInerface
     */
    public function getVatRate()
    {
        return $this->vatRate;
    }

    public function getVatRateCode()
    {
        if ($this->vatRateCode == null) {
            $this->vatRateCode = $this->getVatRate() ? $this->getVatRate()->getCode() : null;
        }

        return $this->vatRateCode;
    }

    public function __sleep()
    {
        $this->getVatRateCode();

        return array('id', 'percent', 'vatRateCode', 'validTo');
    }
}
