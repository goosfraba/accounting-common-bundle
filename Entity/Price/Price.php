<?php
namespace Webit\Accounting\CommonBundle\Entity\Price;

use Webit\Common\DictionaryBundle\Annotation as Dict;
use Webit\Accounting\CommonBundle\Model\Price\PriceInterface;
use Webit\Common\CurrencyBundle\Model\CurrencyInterface;
use Webit\Common\DictionaryBundle\Model\DictionaryItem\DictionaryItemAwareInterface;

/**
 * Webit\Accounting\CommonBundle\Document\Unit\Unit
 * @author dbojdo
 */
class Price implements PriceInterface, DictionaryItemAwareInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var float
     */
    protected $value;

    /**
     * @Dict\ItemCode(dictionaryName="currency",itemProperty="currency")
     */
    protected $currencyCode;

    /**
     *
     * @var CurrencyInterface
     */
    protected $currency;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param float $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return CurrencyInterface
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * @param CurrencyInterface $currency
     */
    public function setCurrency(CurrencyInterface $currency)
    {
        $this->currency = $currency;
    }

    public function __toString()
    {
        return sprintf('%.2f %s', $this->getValue(), $this->getCurrency());
    }
}
