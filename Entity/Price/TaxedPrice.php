<?php
namespace Webit\Accounting\CommonBundle\Entity\Price;

use Webit\Common\DictionaryBundle\Annotation as Dict;
use Webit\Accounting\CommonBundle\Model\Price\TaxedPriceInterface;
use Webit\Accounting\CommonBundle\Model\Vat\VatRateInterface;
use Webit\Accounting\CommonBundle\Calculator\NetGrossCalculator;

class TaxedPrice extends Price implements TaxedPriceInterface
{
    /**
     * @Dict\ItemCode(dictionaryName="vat",itemProperty="vatRate")
     */
    protected $vatRateCode;

    /**
     * @var VatRateInterface
     */
    protected $vatRate;

    /**
     * @var bool
     */
    protected $net = true;

    /**
     * @return VatRateInterface
     */
    public function getVatRate()
    {
        return $this->vatRate;
    }

    /**
     * @param VatRateInterface $vatRate
     */
    public function setVatRate(VatRateInterface $vatRate)
    {
        $this->vatRate = $vatRate;
    }

    /**
     * @return bool
     */
    public function getNet()
    {
        return $this->net;
    }

    /**
     * @param bool $net
     */
    public function setNet($net)
    {
        $this->net = $net;
    }

    /**
     * @return float
     */
    public function getTaxedValue(\DateTime $date = null)
    {
        if ($this->net) {
            $v = NetGrossCalculator::calculateGrossPrice($this, $this->getVatRate(), $date);

            return $v;
        }

        return NetGrossCalculator::calculateNetPrice($this, $this->getVatRate(), $date);
    }
}
