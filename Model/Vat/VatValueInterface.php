<?php
namespace Webit\Accounting\CommonBundle\Model\Vat;

interface VatValueInterface
{
    /**
     * Return float value (ex. 0.23)
     * @return float
     */
    public function getValue();

    /**
     * @param float $value
     */
    public function setValue($value);

    /**
     * Return percent value (ex. 23.00)
     * @return float
     */
    public function getPercent();

    /**
     * @param float $percent
     */
    public function setPercent($percent);

    /**
     * Retrun ratio value (ex. 1.23)
     * @return float
     */
    public function getRatio();

    /**
     * @param float $ratio
     */
    public function setRatio($ratio);

    /**
     * @return \DateTime
     */
    public function getValidTo();

    /**
     * @return VatRateInerface
     */
    public function getVatRate();
}
