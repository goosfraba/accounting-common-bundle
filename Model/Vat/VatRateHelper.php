<?php
namespace Webit\Accounting\CommonBundle\Model\Vat;

class VatRateHelper
{
    /**
     *
     * @param  VatRateInterface $vatRate
     * @param  \DateTime $date
     * @return VatValueInterface
     */
    public static function getValue(VatRateInterface $vatRate, \DateTime $date = null)
    {
        $date = $date ?: new \DateTime();

        $default = null;
        $found = null;

        foreach ($vatRate->getValues() as $value) {
            if (!$value->getValidTo()) {
                $default = $value;
                continue;
            }

            $ts = $value->getValidTo()->getTimestamp();
            $diff = $ts - $date->getTimestamp();

            if ($diff > 0 && (!$found || $ts < $found->getValidTo()->getTimestamp())) {
                $found = $value;
            }
        }

        return $found ?: $default;
    }
}
