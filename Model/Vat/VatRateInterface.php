<?php
namespace Webit\Accounting\CommonBundle\Model\Vat;

use Webit\Common\DictionaryBundle\Model\DictionaryItem\DictionaryItemInterface;

interface VatRateInterface extends DictionaryItemInterface
{
    /**
     *
     * @param  \DateTime $date
     * @return VatValueInterface
     */
    public function getValue(\DateTime $date);

    /**
     * @param VatValueInterface $value
     */
    public function addValue(VatValueInterface $value);

    /**
     * @param VatValueInterface $value
     */
    public function removeValue(VatValueInterface $value);

    /**
     * @return ArrayCollection<VatValueInterface>
     */
    public function getValues();
}
