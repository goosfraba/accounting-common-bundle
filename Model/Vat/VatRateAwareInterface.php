<?php
namespace Webit\Accounting\CommonBundle\Model\Vat;

interface VatRateAwareInterface
{
    /**
     * @return VatRateInterface
     */
    public function getVatRate();

    /**
     *
     * @param VatRateInterface $vatRate
     */
    public function setVatRate(VatRateInterface $vatRate);
}
