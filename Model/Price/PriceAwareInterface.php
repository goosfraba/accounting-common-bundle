<?php
namespace Webit\Accounting\CommonBundle\Model\Price;

interface PriceAwareInterface
{
    public function getPriceNet();

    public function getPriceGross();
}
