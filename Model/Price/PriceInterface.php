<?php
namespace Webit\Accounting\CommonBundle\Model\Price;

use Webit\Common\CurrencyBundle\Model\CurrencyAwareInterface;
use Webit\Common\CurrencyBundle\Model\CurrencyInterface;

/**
 * Immutable Price (no setters - value and currency must be set by constructor)
 * @author dbojdo
 */
interface PriceInterface extends CurrencyAwareInterface
{
    /**
     * @return float
     */
    public function getValue();

    /**
     * @return CurrencyInterface
     */
    public function getCurrency();
}
