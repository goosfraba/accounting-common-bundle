<?php
namespace Webit\Accounting\CommonBundle\Model\Price;

use Webit\Common\CurrencyBundle\Model\CurrencyInterface;

class Price implements PriceInterface
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var float
     */
    protected $value;

    /**
     * @var CurrencyInterface
     */
    protected $currency;

    /**
     * @param float $value
     * @param CurrencyInterface $currency
     */
    public function __construct($value, CurrencyInterface $currency)
    {
        $this->value = $value;
        $this->currency = $currency;
    }

    /**
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return CurrencyInterface
     */
    public function getCurrency()
    {
        return $this->currency;
    }
}
