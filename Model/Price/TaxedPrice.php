<?php
namespace Webit\Accounting\CommonBundle\Model\Price;

use Webit\Accounting\CommonBundle\Calculator\NetGrossCalculator;
use Webit\Accounting\CommonBundle\Model\Vat\VatRateInterface;

class TaxedPrice extends Price
{
    /**
     *
     * @var VatRateInterface
     */
    protected $vatRate;

    /**
     *
     * @var bool
     */
    protected $net = false;

    /**
     * @return \Webit\Accounting\CommonBundle\Model\Vat\VatRateInterface
     */
    public function getVatRate()
    {
        return $this->vatRate;
    }

    /**
     *
     * @param VatRateInterface $vatRate
     */
    public function setVatRate(VatRateInterface $vatRate)
    {
        $this->vatRate = $vatRate;
    }

    /**
     *
     * @param bool $net
     */
    public function setNet($net)
    {
        $this->net = (bool)$net;
    }

    /**
     * @return bool
     */
    public function getNet()
    {
        return $this->net;
    }

    /**
     * @param \DateTime $date
     * @return float
     */
    public function getTaxedValue(\DateTime $date = null)
    {
        if ($this->net) {
            return NetGrossCalculator::calculateGrossPrice($this, $this->getVatRate(), $date);
        }

        return NetGrossCalculator::calculateNetPrice($this, $this->getVatRate(), $date);
    }
}
