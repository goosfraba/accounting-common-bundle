<?php
namespace Webit\Accounting\CommonBundle\Model\Price;

use Webit\Accounting\CommonBundle\Model\Vat\VatRateInterface;

interface TaxedPriceInterface extends PriceInterface
{
    /**
     * @return VatRateInterface
     */
    public function getVatRate();

    /**
     *
     * @param VatRateInterface $vatRate
     */
    public function setVatRate(VatRateInterface $vatRate);

    /**
     * @return bool
     */
    public function getNet();

    /**
     *
     * @param bool $net
     */
    public function setNet($net);

    /**
     * @param  \DateTime $date
     * @return float
     */
    public function getTaxedValue(\DateTime $date = null);
}
