<?php

namespace Webit\Accounting\CommonBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html#cookbook-bundles-extension-config-class}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritDoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('webit_accounting_common');
                $rootNode
                    ->children()
                        ->arrayNode('prices')
                            ->addDefaultsIfNotSet()
                            ->children()
                                ->scalarNode('default_currency')->defaultValue('PLN')->end()
                                ->scalarNode('mode')->defaultValue('net')->end()
                            ->end()
                        ->end()
                        ->arrayNode('vat')->addDefaultsIfNotSet()
                            ->children()
                                ->scalarNode('use_extjs')->defaultTrue()->end()
                                ->scalarNode('dictionary_name')->defaultValue('vat')->end()
                                ->scalarNode('storage_type')->defaultValue('orm')->end()
                                ->arrayNode('orm')->addDefaultsIfNotSet()
                                    ->children()
                                        ->scalarNode('item_class')->defaultValue('Webit\Accounting\CommonBundle\Entity\Vat\VatRate')->end()
                                        ->scalarNode('vat_value_class')->defaultValue('Webit\Accounting\CommonBundle\Entity\Vat\VatValue')->end()
                                    ->end()
                                ->end()
                                ->arrayNode('phpcr')->addDefaultsIfNotSet()
                                    ->children()
                                        ->scalarNode('item_class')->defaultValue('Webit\Accounting\CommonBundle\Document\Vat\VatRate')->end()
                                        ->scalarNode('vat_value_class')->defaultValue('Webit\Accounting\CommonBundle\Document\Vat\VatValue')->end()
                                        ->scalarNode('root')->defaultValue('/vat')->end()
                                    ->end()
                                ->end()
                            ->end()
                    ->end()
                ->end();

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
        return $treeBuilder;
    }
}
