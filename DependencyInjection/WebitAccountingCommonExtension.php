<?php

namespace Webit\Accounting\CommonBundle\DependencyInjection;

use Webit\Common\DictionaryBundle\DependencyInjection\Definition\DictionaryDefinitionHelper;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class WebitAccountingCommonExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $vatLoader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config/vat'));

        $loader->load('services.yml');

        $helper = new DictionaryDefinitionHelper($container, $this->getAlias());

        $dict = $config['vat'];
        $dict['dictionary_name'] = 'vat';
        $dictConfig = $helper->createDictionaryConfig($dict);
        $helper->registerDefinition($dictConfig);

        if ($config['vat']['use_extjs']) {
            $vatLoader->load('extjs.yml');

            $vatConfig = $helper->mergeConfig($config['vat']);
            $def = $container->getDefinition('webit_accounting_common.vat_value_store');
            $def->addArgument($vatConfig['vat_value_class']);

            $dict = $config['vat'];
        }

        $pcp = $container->getDefinition('webit_accounting_common.price_config_provider');
        $pcp->replaceArgument(0, $config['prices']);
    }
}
