<?php
namespace Webit\Accounting\CommonBundle\ExtJs;

use Webit\Bundle\ExtJsBundle\StaticData\StaticDataExposerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class StaticDataExposer implements StaticDataExposerInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    /**
     * @return array<key, data>
     */
    public function getExposedData()
    {
        $staticData = array();

        $staticData['vat_rate'] = $this->container->get('webit_accounting_common.vat_dictionary')->getItems(
        )->getValues();
        foreach ($staticData['vat_rate'] as $rate) {

        }

        return $staticData;
    }
}
