<?php
namespace Webit\Accounting\CommonBundle\ExtJs\Vat;

use Webit\Common\DictionaryBundle\Model\Dictionary\DictionaryInterface;
use Webit\Tools\Object\ObjectUpdater;
use Webit\Bundle\ExtJsBundle\Store\ExtJsStoreAbstract;
use Webit\Bundle\ExtJsBundle\Store\ExtJsJson;
use Webit\Bundle\ExtJsBundle\Store\Sorter\SorterCollectionInterface;
use Webit\Bundle\ExtJsBundle\Store\Filter\FilterCollectionInterface;

class VatRateStore extends ExtJsStoreAbstract
{
    /**
     * @var VatManagerInterface
     */
    protected $vm;

    protected $options;

    public function __construct(DictionaryInterface $vm, $options = array())
    {
        parent::__construct($options);

        $this->vm = $vm;
    }

    /**
     *
     * @param  array $queryParams
     * @param  FilterCollectionInterface $filters
     * @param  SorterCollectionInterface $sorters
     * @param  int $page
     * @param  int $limit
     * @param  int $offset
     * @return ExtJsJson
     */
    public function getModelList(
        $queryParams,
        FilterCollectionInterface $filters,
        SorterCollectionInterface $sorters,
        $page = 1,
        $limit = 25,
        $offset = 0
    ) {
        $rates = $this->vm->getItems();

        $json = new ExtJsJson();
        $json->setData(array_values($rates->toArray()));
        $json->setSerializerGroups(array('Default', 'generic'));

        return $json;
    }

    /**
     *
     * @param  string $id
     * @param $queryParams
     * @return Product
     * @throws \Exception
     */
    public function loadModel($id, $queryParams)
    {
        throw new \Exception('Not implemented');
    }

    public function createModels(\Traversable $arModelList)
    {
        $updater = new ObjectUpdater();
        foreach ($arModelList as $key => $rate) {
            $arModelList[$key] = $this->vm->updateItem($rate);
        }

        $this->vm->commitChanges();

        $json = new ExtJsJson();
        $json->setData($arModelList);
        $json->setSerializerGroups(array('Default', 'generic'));

        return $json;
    }

    public function updateModels(\Traversable $arModelList)
    {
        $updater = new ObjectUpdater();
        foreach ($arModelList as $key => $rate) {
            $mRate = $this->vm->getItem($rate->getSymbol());
            $updater->fromObject($rate, $mRate, array('values', 'symbol'));
            $arModelList[$key] = $this->vm->updateItem($mRate);
        }

        $this->vm->commitChanges();

        $json = new ExtJsJson();
        $json->setData($arModelList);
        $json->setSerializerGroups(array('Default', 'generic'));

        return $json;
    }

    /**
     *
     * @param string $id
     * @return ExtJsJson
     */
    public function deleteModel($id)
    {
        foreach ($id as $rate) {
            $this->vm->removeItem($rate);
        }
        $this->vm->commitChanges();

        $response = new ExtJsJson();
        $response->setData(true);

        return $response;
    }

    public function getDataClass()
    {
        return $this->vm->getItemClass();
    }
}
