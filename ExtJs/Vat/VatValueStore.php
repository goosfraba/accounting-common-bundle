<?php
namespace Webit\Accounting\CommonBundle\ExtJs\Vat;

use Webit\Common\DictionaryBundle\Model\Dictionary\DictionaryInterface;

use Webit\Tools\Object\ObjectUpdater;
use Webit\Bundle\ExtJsBundle\Store\ExtJsStoreAbstract;
use Webit\Bundle\ExtJsBundle\Store\ExtJsJson;
use Webit\Bundle\ExtJsBundle\Store\Sorter\SorterCollectionInterface;
use Webit\Bundle\ExtJsBundle\Store\Filter\FilterCollectionInterface;

class VatValueStore extends ExtJsStoreAbstract
{
    /**
     * @var VatManagerInterface
     */
    protected $vm;

    protected $vatValueClass;

    protected $options;

    public function __construct(DictionaryInterface $vm, $vatValueClass, $options = array())
    {
        parent::__construct($options);
        $this->vm = $vm;
        $this->vatValueClass = $vatValueClass;
    }

    /**
     *
     * @param  array $queryParams
     * @param  FilterCollectionInterface $filters
     * @param SorterCollectionInterface $sorters
     * @param  int $page
     * @param  int $limit
     * @param  int $offset
     * @return ExtJsJson
     * @throws \Exception
     */
    public function getModelList(
        $queryParams,
        FilterCollectionInterface $filters,
        SorterCollectionInterface $sorters,
        $page = 1,
        $limit = 25,
        $offset = 0
    ) {
        if ($symbol = $filters->getFilter('vat_rate_code')) {
            $symbol = $symbol->getValue();
        }

        if (!$symbol) {
            throw new \Exception('Missing required filter: vat_rate_code');
        }

        $rate = $this->vm->getItem($symbol);
        $arValues = array_values($rate->getValues()->toArray());
        foreach ($arValues as &$value) {
            $validTo = $value->getValidTo();
            if ($validTo) {
                $value->setValidTo(new \DateTime($validTo->format('Y-m-d H:i:s')));
            }
        }

        $json = new ExtJsJson();
        $json->setData($arValues);
        $json->setSerializerGroups(array('Default', 'generic'));

        return $json;
    }

    /**
     *
     * @param  string $id
     * @param $queryParams
     * @return Product
     * @throws \Exception
     */
    public function loadModel($id, $queryParams)
    {
        throw new \Exception('Not implemented');
    }

    public function createModels(\Traversable $arModelList)
    {
        $updater = new ObjectUpdater();
        foreach ($arModelList as $key => $value) {
            $rate = $this->vm->getItem($value->getVatRateCode());
            $rate->addValue($value);

            $arModelList[$key] = $value;
        }

        $this->vm->commitChanges();

        $json = new ExtJsJson();
        $json->setData($arModelList);
        $json->setSerializerGroups(array('Default', 'generic'));

        return $json;
    }

    public function updateModels(\Traversable $arModelList)
    {
        $updater = new ObjectUpdater();
        foreach ($arModelList as $key => $value) {
            $rate = $this->vm->getItem($value->getVatRateCode());
            foreach ($rate->getValues() as $mValue) {
                if ($mValue->getId() == $value->getId()) {
                    $updater->fromObject($value, $mValue);
                    $arModelList[$key] = $mValue;
                    break;
                }
            }

            $this->vm->updateItem($rate);
        }

        $this->vm->commitChanges();

        $json = new ExtJsJson();
        $json->setData($arModelList);
        $json->setSerializerGroups(array('Default', 'generic'));

        return $json;
    }

    /**
     *
     * @param string $id
     * @return ExtJsJson
     */
    public function deleteModel($id)
    {
        foreach ($id as $value) {
            $rate = $this->vm->getItem($value->getVatRateCode());
            foreach ($rate->getValues() as $mValue) {
                if ($mValue->getId() == $value->getId()) {
                    $rate->getValues()->removeElement($mValue);
                    break;
                }
            }

            $this->vm->updateItem($rate);
        }

        $this->vm->commitChanges();

        $response = new ExtJsJson();
        $response->setData(true);

        return $response;
    }

    public function getDataClass()
    {
        return $this->vatValueClass;
    }
}
