Ext.define('WebitAccountingCommon.model.Price.Price',{
	extend: 'Ext.data.Model',
	fields: [{
		name: 'id'
	},{
		name: 'currency_code',
		type: 'string'
	},{
		name: 'value',
		type: 'float',
		useNull: true
	},{
		name: 'currency',
		model: 'WebitCommonCurrency.model.Currency',
		persist: false
	}]
});
