Ext.define('WebitAccountingCommon.model.Price.TaxedPrice',{
	extend: 'WebitAccountingCommon.model.Price.Price',
	fields: [{
		name: 'id'
	},{
		name: 'taxed_value',
		type: 'float',
		useNull: true,
		persist: false
	},{
		name: 'vat_rate_code',
		type: 'string'
	},{
		name: 'net',
		type: 'bool',
		defaultValue: true
	},{
		name: 'vat_rate',
		model: 'WebitCommonCurrency.model.VatRate',
		persist: false
	}]
});
