Ext.define('WebitAccountingCommon.model.Vat.VatRate',{
	extend: 'Ext.data.Model',
	fields: [{
		name: 'id'
	},{
		name: 'code',
		type: 'string'
	},{
		name: 'label',
		type: 'string'
	},{
		name: 'value_label',
		type: 'string',
		persist: false
	}],
	hasMany: [{
		model: 'WebitAccountingCommon.model.Vat.VatValue',
		name: 'values',
		foreignKey: 'code',
		associationKey: 'values'
	}],
	getCurrentValue: function() {
		return Ext.create('WebitAccountingCommon.model.Vat.VatValue',this['raw']['values'][0]);
	},
	proxy : {
		type : 'webitrest',
		appendId : false,
		urlSelector : Webit.data.proxy.StoreUrlSelector('webit_accounting_common.vat_rate_store'),
		reader: {
	      type: 'json',
	      root: 'data',
	      successProperty : 'success',
	      totalProperty : 'total',
	      idProperty : 'id'
    	},
    	writer : {
    		type : 'json',
    		writeAllFields : true,
    		allowSingle : false
    	}
	}
});

