Ext.define('WebitAccountingCommon.model.Vat.VatValue',{
	extend: 'Ext.data.Model',
	fields: [{
		name: 'id'
	}
//	,{
//		name: 'vat_rate',
//		model: 'WebitAccountingCommon.model.Vat.VatRate',
//		persist: false
//	}
	,{
		name: 'vat_rate_code'
	},{
		name: 'percent',
		type: 'float',
		useNull: true
	},{
		name: 'valid_to',
		type: 'date',
		dateFormat: 'Y-m-d H:i:s'
	},{
		name: 'ratio',
		type: 'float',
		persist: false
	},{
		name: 'value',
		type: 'float',
		persist: false
	}],
	belongsTo: [{
		model: 'WebitAccountingCommon.model.Vat.VatRate',
		foreignKey: 'vat_rate_code',
		name: 'rate'
	}],
	proxy : {
		type : 'webitrest',
		appendId : false,
		urlSelector : Webit.data.proxy.StoreUrlSelector('webit_accounting_common.vat_value_store'),
		reader: {
	      type: 'json',
	      root: 'data',
	      successProperty : 'success',
	      totalProperty : 'total',
	      idProperty : 'id'
	    },
	    writer : {
	    	type : 'json',
	    	writeAllFields : true,
	    	allowSingle : false
	    }
	}
});
