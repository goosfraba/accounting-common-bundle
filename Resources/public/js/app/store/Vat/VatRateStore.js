Ext.define('WebitAccountingCommon.store.Vat.VatRateStore',{
	extend: 'Ext.data.Store',
	reader: {
		type: 'json'
	},
	sorters: [{
		property: 'code',
		direction: 'ASC'
	}],
	model: 'WebitAccountingCommon.model.Vat.VatRate'
});
