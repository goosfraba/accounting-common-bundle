Ext.define('WebitAccountingCommon.controller.Vat',{
	extend: 'Ext.app.Controller',
	models: [
		'WebitAccountingCommon.view.Vat.MainPanel',
		'WebitAccountingCommon.model.Vat.VatRate',
		'WebitAccountingCommon.model.Vat.VatValue'
	],
	views: [
		'WebitAccountingCommon.view.Vat.VatRateGrid',
		'WebitAccountingCommon.view.Vat.VatValueGrid',
		'WebitAccountingCommon.view.Vat.VatRateCombo'
	],
	stores: [
		'WebitAccountingCommon.store.Vat.VatRateStore'
	],
	init: function() {
		this.control({
			'webit_accounting_common_vat_vat_rate_grid' : {
				afterrender: function(grid) {
					grid.getStore().on('write',function(store, operation) {
						if(operation.action == 'create' && operation.records.length > 0) {
							grid.getSelectionModel().deselectAll();
							grid.getSelectionModel().select(operation.records[0]);
						}
					});
					
					grid.getStore().load();
				},
				selectionchange: this.onRateSelectionChange,
				beforeedit: this.onBeforeRateEdit
			}
		});
	},
	onRateSelectionChange: function(sm, selected) {
		var valueGrid = sm.view.ownerCt.next('webit_accounting_common_vat_vat_value_grid'); 
		if(selected.length > 0 && selected[0].phantom == false) {
			valueGrid.enable();
			valueGrid.getStore().filters.removeAtKey('vat_rate_code');
			valueGrid.getStore().filters.add('vat_rate_code',{property: 'vat_rate_code', value: selected[0].get('code')});
			valueGrid.getStore().load();
		} else {
			valueGrid.disable();
		}
	},
	onBeforeRateEdit: function(editor, e, opts) {
		if(e.record.phantom == false) {
			e.grid.columns[0].getEditor().setReadOnly(true);
		} else {
			e.grid.columns[0].getEditor().setReadOnly(false);
		}
	}
})