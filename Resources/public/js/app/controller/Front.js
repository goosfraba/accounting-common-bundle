Ext.define('WebitAccountingCommon.controller.Front',{
	extend: 'Ext.app.Controller',
	models: [
		'WebitAccountingCommon.model.Price.Price',
		'WebitAccountingCommon.model.Price.TaxedPrice'
	],
	init: function() {
		this.getController('WebitAccountingCommon.controller.Vat').init();
	}
})