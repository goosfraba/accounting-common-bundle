Ext.define('WebitAccountingCommon.view.Vat.VatRateGrid',{
	extend: 'Webit.view.grid.EditableGrid',
	alias: 'widget.webit_accounting_common_vat_vat_rate_grid',
	editmode: 'row',
	title: 'Stawki',
	initComponent: function() {
		Ext.apply(this,{
			store: {
				model: 'WebitAccountingCommon.model.Vat.VatRate',
				autoSync: true,
				sorters: [{
					property: 'code',
					direction: 'ASC'
				}],
				remoteSort: false
			},
			columns: [{
				header: 'Symbol',
				dataIndex: 'code',
				width: 100,
				editor: {
					xtype: 'textfield',
					allowBlank: false
				}
			},{
				header: 'Opis',
				dataIndex: 'label',
				flex: 1,
				editor: {
					xtype: 'textfield',
					allowBlank: false
				}
			}]
		});
		
		this.callParent();
	}
});