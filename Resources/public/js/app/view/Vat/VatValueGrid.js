Ext.define('WebitAccountingCommon.view.Vat.VatValueGrid',{
	extend: 'Webit.view.grid.EditableGrid',
	alias: 'widget.webit_accounting_common_vat_vat_value_grid',
	editmode: 'row',
	title: 'Wartości',
	initComponent: function() {
		Ext.apply(this,{
			store: {
				model: 'WebitAccountingCommon.model.Vat.VatValue',
				remoteFilter: true,
				remoteSort: false,
				sorters: [{
					property: 'valid_to',
					direction: 'ASC'
				}],
				autoSync: true
			},
			columns: [{
				header: 'Procent',
				dataIndex: 'percent',
				width: 100,
				editor: {
					xtype: 'numberfield',
					allowBlank: false
				}
			},{
				header: 'Obowiązuje do',
				dataIndex: 'valid_to',
				width: 150,
				editor: {
					xtype: 'datefield',
					format: 'Y-m-d H:i:s',
					allowBlank: false
				},
				renderer: Ext.util.Format.dateRenderer('Y-m-d H:i:s')
			},{
				header: 'Wartość',
				dataIndex: 'value',
				width: 100
			},{
				header: 'Mnożnik',
				dataIndex: 'ratio',
				flex: 1
			}]
		});
		
		this.callParent();
	},
	getModelDefaults: function() {
		var sel = this.prev('webit_accounting_common_vat_vat_rate_grid').getSelectionModel().getSelection();
		var validTo = Ext.Date.add(new Date(),Ext.Date.YEAR,20);
		validTo.setMonth(11,31);
		validTo.setHours(23,59,59);
		
		return {
			vat_rate_code: sel.length > 0 ? sel[0].get('code') : null,
			valid_to: validTo
		};
	}
});
