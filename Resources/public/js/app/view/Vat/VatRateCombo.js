Ext.define('WebitAccountingCommon.view.Vat.VatRateCombo',{
	extend: 'Ext.form.ComboBox',
	alias: 'widget.webit_accounting_common_vat_vat_rate_combo',
	fieldLabel: 'Jednostka',
	valueField: 'code',
	displayField: 'value_label',
	forceSelection: true,
	editable: false,
	dataMode: 'ajax',
	initComponent: function() {
		if(this.dataMode == 'ajax') {
			Ext.apply(this,{
				store: 'WebitAccountingCommon.store.Vat.VatRateStore'
			});
		} else {
			Ext.apply(this,{
				queryMode: 'local',
				store: {
					model: 'WebitAccountingCommon.model.Vat.VatRate',
					data: Webit.data.StaticData.getData('vat_rate')
				}
			});
		}
		
		this.callParent();
	}
});
