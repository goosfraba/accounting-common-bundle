Ext.define('WebitAccountingCommon.view.Vat.MainPanel',{
	extend: 'Ext.panel.Panel',
	alias: 'widget.webit_accounting_common_vat_main_panel',
	initComponent: function() {
		Ext.apply(this,{
			layout: {
				type: 'hbox',
				align: 'stretch'
			},
			items: [{
				xtype: 'webit_accounting_common_vat_vat_rate_grid',
				flex: 1
			},{
				xtype: 'webit_accounting_common_vat_vat_value_grid',
				flex: 1,
				disabled: true
			}]
		});
		
		this.callParent();
	}
});
