<?php
namespace Webit\Accounting\CommonBundle\Calculator;

use Webit\Accounting\CommonBundle\Model\Vat\VatRateInterface;
use Webit\Accounting\CommonBundle\Model\Price\PriceInterface;

class NetGrossCalculator
{
    /**
     *
     * @param  PriceInterface $gross
     * @param  VatRateInterface $vat
     * @param  \DateTime $date
     * @return float
     */
    public static function calculateNetPrice(
        PriceInterface $gross,
        VatRateInterface $vat = null,
        \DateTime $date = null
    ) {
        $date = $date ?: new \DateTime();
        if ($vat && $vatValue = $vat->getValue($date)) {
            $value = $gross->getValue() / $vatValue->getRatio();
        } else {
            $value = $gross->getValue();
        }

        return $value;
    }

    /**
     *
     * @param  PriceInterface $net
     * @param  VatRateInterface $vat
     * @param  \DateTime $date
     * @return float
     */
    public static function calculateGrossPrice(
        PriceInterface $net,
        VatRateInterface $vat = null,
        \DateTime $date = null
    ) {
        $date = $date ?: new \DateTime();
        if ($vat && $vatValue = $vat->getValue($date)) {
            $value = $net->getValue() * $vatValue->getRatio();
        } else {
            $value = $net->getValue();
        }

        return $value;
    }
}
